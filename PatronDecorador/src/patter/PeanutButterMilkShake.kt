package patter

public class PeanutButterMilkShake(m:MilkShake) : MilkShakeDecorator(m){

    override public fun getTaste(){
        super.getTaste ();
        this.addTaste();
        println(" Es un batido de mantequilla de maní. !");
    }
    public fun addTaste(){
        println(" Añadiendo sabor a mantequilla de maní al batido. !");
    }
}
