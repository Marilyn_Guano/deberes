package patter

import kotlin.properties.Delegates

class ObservableObject (listener: ValueChangeListener){
    var text: String by Delegates.observable(
        initialValue = "",
        onChange = {
            prop, old, new->listener.onValueChanged(new)
        }
    )

}